import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter as Router, Route } from "react-router-dom";
import PostView from "./components/postView";
import Header from "./components/header";

var app = document.getElementById("root");

ReactDOM.render(
  <div className="App">
    <Header />
    <Router history="hashHistory">
      <div>
        <Route path="/" component={App} exact />
        <Route path="/post" component={PostView} exact />
      </div>
    </Router>
  </div>,
  app
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
