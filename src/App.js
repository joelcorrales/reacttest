import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import AppBody from "./components/appBody";

class App extends Component {
  render() {
    return (
      <div>
        <AppBody />
      </div>
    );
  }
}

export default App;
