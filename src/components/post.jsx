import React, { Component } from "react";
import moment from "moment";
import { Link } from "react-router-dom";

const imageStyle = {
  height: "225px",
  width: "100%",
  display: "block"
};
class Post extends Component {
  state = {};
  postLife = () => {
    let creationDate = new Date(this.props.created);
    return moment(creationDate).fromNow();
  };
  render() {
    return (
      <div className="col-md-4">
        <div className="card mb-4 shadow-sm">
          <img
            className="card-img-top"
            alt="Thumbnail [100%x225]"
            style={imageStyle}
            src={this.props.imageSrc["downsized_still"].url}
            data-holder-rendered="true"
          />
          <div className="card-body">
            <p className="card-text">
              <strong>Title:</strong> {this.props.title}
              <br />
              <strong>Autor:</strong> {this.props.user}
            </p>
            <div className="d-flex justify-content-between align-items-center">
              <div className="btn-group">
                <Link to="post" post={this.props.imageSrc}>
                  <button
                    type="button"
                    className="btn btn-sm btn-outline-secondary"
                  >
                    View
                  </button>
                </Link>
                <button
                  type="button"
                  className="btn btn-sm btn-outline-secondary"
                >
                  Edit
                </button>
              </div>
              <small className="text-muted">{this.postLife()}</small>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Post;
