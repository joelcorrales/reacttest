import React, { Component } from "react";
class Header extends Component {
  constructor() {
    super();
    this.state = {
      name: "Joel Corrales",
      phone: "+5493413121475",
      email: "joel.corrales@outlook.com",
      isActive: false
    };
  }

  formatPhone = () => {
    return this.state.phone.replace(
      /(\d\d\d)(\d\d\d)(\d\d\d)(\d\d\d\d)/,
      "$1 $2-$3-$4"
    );
  };
  showInfo = () => {
    this.setState({
      isActive: !this.state.isActive
    });
    console.log(this.state.isActive);
  };
  render() {
    return (
      <header>
        <div
          className={"collapse bg-dark " + (this.state.isActive ? "show" : "")}
          id="navbarHeader"
        >
          <div className="container">
            <div className="row">
              <div className="col-sm-8 col-md-7 py-4">
                <h4 className="text-white">About</h4>
                <p className="text-muted">
                  This is a simple app to start using React architecture and
                  endpoint calls combined with some simple components and other
                  libraries to simulate a complete react work experience.
                </p>
              </div>
              <div className="col-sm-4 offset-md-1 py-4">
                <h4 className="text-white">Developer</h4>
                <ul className="list-unstyled">
                  <li>
                    <a href="#" className="text-white">
                      {this.state.name}
                    </a>
                  </li>
                  <li>
                    <a href="#" className="text-white">
                      {this.formatPhone()}
                    </a>
                  </li>
                  <li>
                    <a
                      href={"mailto:" + this.state.email}
                      className="text-white"
                    >
                      Email me {this.state.email}
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="navbar navbar-dark bg-dark shadow-sm">
          <div className="container d-flex justify-content-between">
            <a href="/" className="navbar-brand d-flex align-items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="mr-2"
              >
                <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />
                <circle cx="12" cy="13" r="4" />
              </svg>
              <strong>Album</strong>
            </a>
            <button
              className="navbar-toggler"
              type="button"
              onClick={this.showInfo}
            >
              <span className="navbar-toggler-icon" />
            </button>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
