import React, { Component } from "react";
import Post from "./post";
import Request from "superagent";

class AppBody extends Component {
  state = {
    posts: []
  };
  loadPost = () => {
    const url =
      "http://api.giphy.com/v1/gifs/trending?api_key=LS0KLLvThNi8vPB6QPRGn7jppkbNLeKP&limit=10";
    Request.get(url).then(resp => {
      this.setState({
        posts: resp.body.data
      });
    });
  };
  render() {
    return (
      <div className="container">
        <br />
        <button className="btn btn-primary" onClick={this.loadPost}>
          Load Posts!
        </button>
        <br />
        <br />
        <br />
        <div className="row">
          {this.state.posts.map(post => (
            <Post
              imageSrc={post.images}
              key={post.id}
              created={post.import_datetime}
              title={post.title}
              user={post.user.display_name}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default AppBody;
